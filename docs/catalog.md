## Informasi Status Peserta 

Menampilkan nama, jenis kelamin, tanggal lahir, status, jenis peserta, kelas, dan nama faskes peserta.

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/JeniRest/kepesertaan/cekstatus/(parameter)"
```

```
parameter harus numeric atau angka; parameter harus berjumlah 13 atau 16 digit
```

`success response`
```json
{
    "metaData": {
        "code": "200",
        "message": "OK"
    },
    "response": {
        "nmPeserta": "MUHAMMADDIN FAISAL",
        "jnskelPeserta": "Laki-laki",
        "tglLhrPeserta": "1977-08-21",
        "nmStatusPeserta": "Aktif",
        "jnsPeserta": "PEKERJA MANDIRI",
        "klsPeserta": "3",
        "nmPPK": "PUSKESMAS CIPUTAT"
    }
}

```

---

## Informasi Status Anggota Keluarga

Menampilkan nama, jenis kelamin, tanggal lahir, status, jenis peserta, kelas, dan nama faskes anggota keluarga.

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/JeniRest/kepesertaan/cekkeluarga/(parameter)"
```

```
parameter harus numeric atau angka; parameter harus berjumlah 13 atau 16 digit
```

`success response`
```json
{
   "metaData": {
       "code": "200",
       "message": "Ok"
   },
   "response": {
       "list": [
           {
               "nokapstKepalaKeluarga": "0001270533699",
               "nmKepalaKeluarga": "MUHAMMADDIN FAISAL",
               "jnskelKepalaKeluarga": "Laki-laki",
               "nmHubKelKepalaKeluarga": "PESERTA",
               "nmPPKKepalaKeluarga": "PUSKESMAS CIPUTAT",
               "nokapstAgtKeluarga": "0001270533699",
               "nmAgtKeluarga": "MUHAMMADDIN FAISAL",
               "jnskelAgtKeluarga": "Laki-laki",
               "nmHubKelAgtKeluarga": "PESERTA",
               "nmPPKAgtKeluarga": "PUSKESMAS CIPUTAT",
               "nmStatusAgtKeluarga": "Aktif"
           },
           {
               "nokapstKepalaKeluarga": "0001270533699",
               "nmKepalaKeluarga": "MUHAMMADDIN FAISAL",
               "jnskelKepalaKeluarga": "Laki-laki",
               "nmHubKelKepalaKeluarga": "PESERTA",
               "nmPPKKepalaKeluarga": "PUSKESMAS CIPUTAT",
               "nokapstAgtKeluarga": "0001492955223",
               "nmAgtKeluarga": "JOUKEY",
               "jnskelAgtKeluarga": "Laki-laki",
               "nmHubKelAgtKeluarga": "ANAK",
               "nmPPKAgtKeluarga": "PUSKESMAS CIPUTAT",
               "nmStatusAgtKeluarga": "Aktif"
           },
           {
               "nokapstKepalaKeluarga": "0001270533699",
               "nmKepalaKeluarga": "MUHAMMADDIN FAISAL",
               "jnskelKepalaKeluarga": "Laki-laki",
               "nmHubKelKepalaKeluarga": "PESERTA",
               "nmPPKKepalaKeluarga": "PUSKESMAS CIPUTAT",
               "nokapstAgtKeluarga": "0001492955256",
               "nmAgtKeluarga": "JOUKEYE",
               "jnskelAgtKeluarga": "Laki-laki",
               "nmHubKelAgtKeluarga": "ANAK",
               "nmPPKAgtKeluarga": "PUSKESMAS CIPUTAT",
               "nmStatusAgtKeluarga": "Aktif"
           }
       ]
   }
}
```

---

## Informasi Nomor Virtual Account

Menampilkan nomor virtual account peserta.

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/JeniRest/kepesertaan/cekva/(parameter)"
```

```
parameter harus numeric atau angka; parameter harus berjumlah 13 atau 16 digit
```

`success response`
```json
{
   "metaData": {
       "code": "200",
       "message": "Ok"
   },
   "response": {
       "noVA": "8888801270533699"
   }
}
```

---

## Informasi Iuran dan Status Pembayaran Peserta 

Menampilkan iuran dan status pembayaran peserta

```
header

X-cons-id : {consid}
X-timestamp : {timestamp}
X-signature : {signature}
```

```
method : get
url : "{BASE_URL}/JeniRest/iuran/cekstatus/(parameter)"
```

```
parameter harus numeric atau angka; parameter harus berjumlah 13 atau 16 digit
```

`success response`
```json
{
   "metaData": {
       "code": "200",
       "message": "Ok"
   },
   "response": {
       "premi": "0",
       "status": "Lunas"
   }
}
```